package mathmodeler;

public class ORToolsManager {

	public static void loadLibraries() {
		String os_name = System.getProperty("os.name").toLowerCase();
		String os_arch = System.getProperty("os.arch").toLowerCase();
		if (os_name.indexOf("win") >= 0) {
			System.loadLibrary("jniortools");
		} else if (os_name.indexOf("linux") >= 0) {
			System.loadLibrary("jniortools_x86_64");
		} else if (os_name.indexOf("mac") >= 0) {
			System.loadLibrary("jniortools");
		} else {
			System.out.println("unknown OS " + os_name);
		}

	}
}

package solvers.math;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.google.ortools.linearsolver.MPSolver;

import core.HCSProblem;
import core.Importer;
import core.IntDoubleAscendingComparator;
import core.IntDoublePair;
import core.Solution;
import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.monitor.EtmPoint;
import etm.core.renderer.SimpleTextRenderer;
import gurobi.GRB;
import gurobi.GRBException;
import mathmodeler.DecisionVariable;
import mathmodeler.LPConstraint;
import mathmodeler.LPExpression;
import mathmodeler.MathAgentGurobiCplex;
import mathmodeler.Objective;
import solvers.heuristics.MinMinPlus;

public class MathSolverLPColumnPricing {
	private static final EtmMonitor etmMonitor = EtmManager.getEtmMonitor();
	private static EtmMonitor monitor;

	private static Logger log = LoggerFactory
			.getLogger(MathSolverLPColumnPricing.class);

	public static void main(String[] args) throws IOException, GRBException {
		BasicEtmConfigurator.configure();
		monitor = EtmManager.getEtmMonitor();
		monitor.start();

		// String fn = "datasets//toy.0"; // LB=16,7827
		String fn = "datasets//Braun_et_al//u_c_hihi.0"; // LB=7346524,1830
		// String fn = "datasets//1024x32//A.u_c_hihi"; // LB=19449229,9980

		// String fn = "datasets//2048x64//A.u_c_hihi"; // LB=17141977,4339
		// String fn = "datasets//2048x64//A.u_i_lolo"; // LB=25,1470

		// String fn = "datasets//4096x128//A.u_c_hihi"; // LB=14829360,5802
		// String fn = "datasets//4096x128//B.u_i_hihi"; // **LB=374988,9

		// String fn = "datasets//8192x256//A.u_c_hihi"; // LB=13338612,53
		// String fn = "datasets//8192x256//A.u_i_hihi"; // ** GLPK crashes !!!
		// String fn = "datasets//8192x256//A.u_i_lohi"; // **
		// String fn = "datasets//8192x256//A.u_i_lolo"; // **
		// String fn = "datasets//8192x256//B.u_i_lolo"; // **LB = 64,1

		long random_seed = 1234567890L;
		Importer importer = new Importer(fn);
		HCSProblem problem = importer.read_dataset();
		MathSolverLPColumnPricing app = new MathSolverLPColumnPricing(problem,
				random_seed);
		// Solution sol = app.solveUsingColumnPricing(90, "gurobi");
		// log.info(sol.toString());

		app.setYVarsLimit(20000);
		app.setExtraVarsLimit(20);
		app.setSolverType("gurobi");
		// app.setSolverType("ortools_glpk");
		// app.setSolverType("ortools_cbc");
		app.setTimeLimit(3600);
		String s = app.findLPBoundUsingColumnPricing();
		log.info(s);

		etmMonitor.render(new SimpleTextRenderer());
		monitor.stop();

	}

	private HCSProblem problem;
	private MathAgentGurobiCplex mathAgent;
	private List<Integer> processor_indices;
	private List<Integer> task_indices;
	private Random random;
	private TaskProcessorPairsAgent tppa;
	private int y_vars_limit;
	private int limit_vars_per_task;
	private String solverType;
	private int timeLimit;

	Map<Integer, DecisionVariable> cacheDV_Y;
	DecisionVariable cachedDV_m;
	Objective cachedObjective;
	LPConstraint[] cachedConstraint;

	public MathSolverLPColumnPricing(HCSProblem problem, long random_seed) {
		this.problem = problem;
		this.random = new Random(random_seed);
		processor_indices = new ArrayList<>(problem.P);
		for (int p = 0; p < problem.P; p++)
			processor_indices.add(p);
		task_indices = new ArrayList<>(problem.T);
		for (int t = 0; t < problem.T; t++)
			task_indices.add(t);
		y_vars_limit = 20000;
		limit_vars_per_task = 20;
		cacheDV_Y = new HashMap<>();
		cachedConstraint = new LPConstraint[problem.T + problem.P];
	}

	public MathAgentGurobiCplex getMathAgent() {
		return mathAgent;
	}

	public Solution solveUsingColumnPricing(int time_limit_in_secs)
			throws GRBException {
		return solveUsingColumnPricing(time_limit_in_secs, "Gurobi");
	}

	@Deprecated
	public Solution solveUsingColumnPricing(int time_limit_in_secs,
			String solverType) throws GRBException {
		Stopwatch timer = Stopwatch.createStarted();
		Solution bestSolution;
		// [1] generate initial heuristic solution
		Solution solution = getInitialHeuristicSolution();
		log.info(solution.toString());
		bestSolution = solution.copy();

		// [2] select variables y_tp included in the solution and
		// randomly select other variables to be included
		initializeTppa(solution);

		int round = 0;
		do {
			round++;
			log.debug(String.format(
					"Round %d time Best IP=%.2f remaining %dsecs", round,
					bestSolution.makespan(),
					time_limit_in_secs - timer.elapsed(TimeUnit.SECONDS)));
			mathAgent = new MathAgentGurobiCplex();
			mathAgent.setEnableOutput(false);
			mathAgent.setTimeLimit(20);
			mathAgent.setModelName("HCSP_" + round);
			formulateProblem();

			if (solverType.equalsIgnoreCase("Gurobi")) {
				mathAgent.prepareGurobi();
				mathAgent.solveUsingGurobi();
				mathAgent.computeDualsUsingGurobi();
				// mathAgent.print_X_RC_ForBasisVariables();
			} else if (solverType.equalsIgnoreCase("ORTools")) {
				mathAgent.prepareORTools(MPSolver.OptimizationProblemType.GLPK_LINEAR_PROGRAMMING);
				mathAgent.solveUsingORTools();
				mathAgent.computeDualsUsingORTools();
			}

			solution = getIPSolutionUsingHeuristics();
			double perc = 100
					* (solution.makespan() - mathAgent.getObjectiveValue())
					/ mathAgent.getObjectiveValue();
			log.debug(String.format("IP Solution %.4f distance from LP %.6f%%",
					solution.makespan(), perc));
			improve1(solution);
			perc = 100 * (solution.makespan() - mathAgent.getObjectiveValue())
					/ mathAgent.getObjectiveValue();
			log.debug(String.format(
					"IP Solution improved %.4f distance from LP %.6f%%",
					solution.makespan(), perc));
			improve2(solution);
			perc = 100 * (solution.makespan() - mathAgent.getObjectiveValue())
					/ mathAgent.getObjectiveValue();
			log.debug(String.format(
					"IP Solution improved swap %.4f distance from LP %.6f%%",
					solution.makespan(), perc));

			if (solution.makespan() < bestSolution.makespan()) {
				log.debug(String.format("Better solution found by %.6f",
						solution.makespan() - bestSolution.makespan()));
				bestSolution = solution.copy();
			}

			if (mathAgent.getObjectiveValue() - solution.makespan() > 0.0001) {
				log.debug(String.format("IP HEURISTIC FOUND A BETTER SOLUTION"));
				initializeTppa(solution);
			} else {
				// remove non-basic variables
				int c = 0;
				for (DecisionVariable dv : mathAgent.getDecisionVariables("y")) {
					String status = null;
					if (solverType.equalsIgnoreCase("Gurobi")) {
						status = mathAgent
								.getBasisStatusForVariableUsingGurobi(dv);
					} else if (solverType.equalsIgnoreCase("ORTools")) {
						status = mathAgent
								.getBasisStatusForVariableUsingORTools(dv);
					}
					if (status.equalsIgnoreCase("NON_BASIC")) {
						String[] s = dv.getIndices().split("_");
						int t = Integer.parseInt(s[0]);
						int p = Integer.parseInt(s[1]);
						tppa.removePair(t, p);
						c++;
					}
				}
				log.debug(String.format("%d non basic variables removed", c));
				// add variables based on dual values + reduced costs
				int vars_rc_zero = updateTppaBasedOnDualValues();

				if (solverType.equalsIgnoreCase("Gurobi")) {
					mathAgent.disposeUsingGurobi();
				} else if (solverType.equalsIgnoreCase("ORTools")) {
					mathAgent.disposeUsingORTools();
				}

				if (vars_rc_zero == 0) {
					log.debug("LP found. Number of variables with negative zero cost = 0");
					Solution sol = constructSolutionBasedOnBest(bestSolution);
					initializeTppa(sol);
					// break;
				}
			}
		} while (timer.elapsed(TimeUnit.SECONDS) < time_limit_in_secs);
		timer.stop();
		log.debug("Time elapsed: " + timer.toString());
		return bestSolution;
	}

	private Solution constructSolutionBasedOnBest(Solution bestSolution) {
		double turbulancePercentage = 0.1;
		Solution sol = bestSolution.copy();
		for (int i = 0; i < turbulancePercentage * problem.T; i++) {
			int rt = random.nextInt(problem.T);
			int rp = random.nextInt(problem.P);
			int p = sol.getProcessorForTask(rt);
			sol.unschedule(rt, p);
			sol.schedule(rt, rp);
		}
		log.debug("New initial sol. Makespan = " + sol.makespan());
		return sol;
	}

	// move tasks from pmax to other processor
	private void improve1(Solution solution) {

		Collections.shuffle(processor_indices, random);
		boolean done = false;
		outer_loop: while (!done) {
			done = true;
			int pmax = solution.getProcessorWithLatestFinishTime();
			double makespan = solution.makespan();
			for (Integer t : solution.getTasksPerProcessor(pmax)) {
				for (Integer p : processor_indices) {
					if (p == pmax)
						continue;
					if (solution.getProcessorFinishTime(p)
							+ problem.getETC(t, p) < makespan) {
						solution.unschedule(t, pmax);
						solution.schedule(t, p);
						// log.debug(String
						// .format("Task %d moved from %d to %d new makespan %.2f",
						// t, pmax, p, solution.makespan()));
						done = false;
						continue outer_loop;
					}
				}
			}
		}
	}

	// swap tasks between pmax and another processor
	private void improve2(Solution solution) {
		Collections.shuffle(processor_indices, random);
		int tries_limit = problem.P * problem.T * 2;
		int c = 0;
		boolean done = false;
		outer_loop: while (!done) {
			if (c > tries_limit)
				break;
			done = true;
			int pmax = solution.getProcessorWithLatestFinishTime();
			double makespan = solution.makespan();
			for (Integer t : solution.getTasksPerProcessor(pmax)) {
				for (Integer p : processor_indices) {
					if (p == pmax)
						continue;
					for (Integer t2 : solution.getTasksPerProcessor(p)) {
						c++;
						double v1 = solution.getProcessorFinishTime(p)
								+ problem.getETC(t, p) - problem.getETC(t2, p);
						double v2 = solution.getProcessorFinishTime(pmax)
								+ problem.getETC(t2, pmax)
								- problem.getETC(t, pmax);
						if ((v1 < makespan) && (v2 < makespan)) {
							solution.unschedule(t, pmax);
							solution.unschedule(t2, p);
							solution.schedule(t, p);
							solution.schedule(t2, pmax);
							// log.debug(String
							// .format("Tasks %d %d swaped between %d and %d %.2f %.2f",
							// t, t2, pmax, p, v1, v2));
							done = false;
							continue outer_loop;
						}
					}
				}
			}
		}

	}

	private Solution getIPSolutionUsingHeuristics() {
		Solution sol = new Solution(problem);
		List<IntDoublePair>[] aList = new List[problem.T];
		for (int t = 0; t < problem.T; t++) {
			aList[t] = new ArrayList<IntDoublePair>();
		}
		for (DecisionVariable dv : mathAgent.getDecisionVariables("y")) {
			String[] s = dv.getIndices().split("_");
			int t = Integer.parseInt(s[0]);
			int p = Integer.parseInt(s[1]);
			double value = dv.getValue();
			if (value > 0.00001)
				aList[t].add(new IntDoublePair(p, value));
		}
		for (int t = 0; t < problem.T; t++) {
			if (aList[t].size() > 1) {
				Collections.sort(aList[t]);
				double rv = random.nextDouble();
				double sum = 0.0;
				int i = 0;
				do {
					sum += aList[t].get(i).doubleValue;
					i++;
				} while (sum < rv);
				sol.schedule(t, aList[t].get(i - 1).intValue);
			} else {
				sol.schedule(t, aList[t].get(0).intValue);
			}
		}
		return sol;
	}

	private int updateTppaBasedOnDualValues() {
		EtmPoint point = etmMonitor
				.createPoint("MathSolverLPColumnPricing:updateTppaBasedOnDualValues");
		double[] duals = mathAgent.getDuals();

		List<IntDoublePair>[] aTList = new ArrayList[problem.T];
		int vars_neqative_rc = 0;
		for (int t = 0; t < problem.T; t++) {
			aTList[t] = new ArrayList<IntDoublePair>();
			for (int p = 0; p < problem.P; p++) {
				if (tppa.checkIfExists(t, p))
					continue;
				double rc = 0 - duals[t] + problem.getETC(t, p)
						* duals[problem.T + p];
				if (rc < -0.00001) {
					aTList[t].add(new IntDoublePair(p, rc));
					vars_neqative_rc++;
				}
			}
			if (aTList[t].size() > 1) {
				Collections.shuffle(aTList[t], random);
				Collections
						.sort(aTList[t], new IntDoubleAscendingComparator());

				// Set<Double> values = new HashSet<Double>();
				// for (IntDoubleTuple idp : aTList[t]) {
				// values.add(idp.doubleValue);
				// }
				// log.debug(String.format(
				// "Negative rc variables for task %d are %d %s", t,
				// aTList[t].size(), values));
			}
		}

		int extra_vars = y_vars_limit - tppa.getSize();
		Collections.shuffle(task_indices, random);
		int c = 0;

		for (Integer t : task_indices) {
			int p;
			if (aTList[t].size() > limit_vars_per_task) {
				for (int j = 0; j < limit_vars_per_task; j++) {
					p = aTList[t].get(j).intValue;
					// if (!tppa.checkIfExists(t, p)) {
					tppa.addPair(t, p);
					c++;
					// }
				}
			} else {
				for (IntDoublePair idt : aTList[t]) {
					p = idt.intValue;
					// if (!tppa.checkIfExists(t, p)) {
					tppa.addPair(t, p);
					c++;
					// }
				}
			}
			if (c > extra_vars)
				break;
		}

		tppa.printStatistics();
		log.debug("negative rc " + vars_neqative_rc);
		point.collect();
		return c;
	}

	private boolean checkOptimalityConditions() {
		double[] duals = mathAgent.getDuals();
		for (int t = 0; t < problem.T; t++) {
			for (int p = 0; p < problem.P; p++) {
				if (tppa.checkIfExists(t, p))
					continue;
				double rc = 0 - duals[t] + problem.getETC(t, p)
						* duals[problem.T + p];
				if (rc < 0.0) {
					return false;
				}
			}
		}
		return true;
	}

	private void initializeTppa(Solution initial_solution) {
		EtmPoint point = etmMonitor
				.createPoint("MathSolverLPColumnPricing:initializeTppa");
		tppa = new TaskProcessorPairsAgent(problem);
		for (int t = 0; t < problem.T; t++) {
			tppa.addPair(t, initial_solution.getProcessorForTask(t));
		}
		// add random variables until Y_VARS_LIMIT
		int extra_vars_per_task = (y_vars_limit - problem.T) / problem.T;
		if (extra_vars_per_task > problem.P)
			extra_vars_per_task = problem.P;
		for (int t = 0; t < problem.T; t++) {
			Collections.shuffle(processor_indices, random);
			int i = 0;
			int c = 0;
			while (c < extra_vars_per_task) {
				if (!tppa.taskToListOfProcessorsMap[t]
						.contains(processor_indices.get(i))) {
					tppa.addPair(t, processor_indices.get(i));
					c++;
				}
				i++;
				if (i == problem.P)
					break;
			}
		}
		point.collect();
	}

	boolean firstPass = false;

	private void formulateProblem() {
		EtmPoint point = etmMonitor
				.createPoint("MathSolverLPColumnPricing:formulateProblem");
		generateVariables1();
		generateVariables2();
		if (!firstPass) {
			firstPass = true;
			generateObjective_FirstPass();
			generateConstraints1_FirstPass();
			generateConstraints2_FirstPass();
		} else {
			generateObjective();
			generateConstraints1();
			generateConstraints2();
		}
		point.collect();
	}

	private void generateVariables1() {
		String key = "y";
		List<DecisionVariable> aList = new ArrayList<>();
		for (int t = 0; t < problem.T; t++)
			for (Integer p : tppa.taskToListOfProcessorsMap[t]) {
				int varno = t * problem.P + p;
				if (cacheDV_Y.containsKey(varno)) {
					aList.add(cacheDV_Y.get(varno));
				} else {
					String varname = String.format("y%d_%d", t, p);
					String indices = String.format("%d_%d", t, p);
					DecisionVariable dv = new DecisionVariable(varname,
							indices, "REAL");
					dv.setLb(0.0);
					dv.setUb(1.0);
					cacheDV_Y.put(varno, dv);
					aList.add(dv);
				}
			}
		mathAgent.addIndexedDecisionVariables(key, aList);
	}

	public void generateVariables2() {
		List<DecisionVariable> aList = new ArrayList<>();
		String key = "m";
		if (cachedDV_m == null) {
			String varname = "m";
			String indices = "0";
			DecisionVariable dv = new DecisionVariable(varname, indices, "REAL");
			dv.setLb(0);
			dv.setUb(Double.MAX_VALUE);
			aList.add(dv);
		} else {
			aList.add(cachedDV_m);
		}
		mathAgent.addIndexedDecisionVariables(key, aList);
	}

	public void generateObjective_FirstPass() {
		LPExpression expr = new LPExpression();
		DecisionVariable dv = mathAgent.getIndexedDecisionVariable("m", "0");
		expr.addTerm(1.0, dv);
		Objective objective = new Objective("Minimize");
		objective.setExpr(expr);
		mathAgent.setObjective(objective);
		cachedObjective = objective;
	}

	public void generateObjective() {
		mathAgent.setObjective(cachedObjective);
	}

	private void generateConstraints1_FirstPass() {
		for (int t = 0; t < problem.T; t++) {
			String constraint_name = String.format("c1_t%d", t);
			LPExpression expr = new LPExpression();
			for (Integer p : tppa.taskToListOfProcessorsMap[t]) {
				// String indices = String.format("%d_%d", t, p);
				// DecisionVariable dv = mathAgent.getDecisionVariable("y",
				// indices);
				DecisionVariable dv = cacheDV_Y.get(t * problem.P + p);
				expr.addTerm(1.0, dv);
			}
			LPConstraint constraint = new LPConstraint(constraint_name);
			constraint.setExpression(expr);
			constraint.setLhs(1.0);
			constraint.setRhs(1.0);
			mathAgent.addConstraint(constraint);
			cachedConstraint[t] = constraint;
		}
	}

	private void generateConstraints1() {
		for (int t = 0; t < problem.T; t++) {
			LPExpression expr = new LPExpression();
			for (Integer p : tppa.taskToListOfProcessorsMap[t]) {
				// String indices = String.format("%d_%d", t, p);
				// DecisionVariable dv = mathAgent.getDecisionVariable("y",
				// indices);
				DecisionVariable dv = cacheDV_Y.get(t * problem.P + p);
				expr.addTerm(1.0, dv);
			}
			LPConstraint constraint = cachedConstraint[t];
			constraint.setExpression(expr);
			mathAgent.addConstraint(constraint);
		}
	}

	private void generateConstraints2() {
		for (int p = 0; p < problem.P; p++) {
			LPExpression expr = new LPExpression();
			// DecisionVariable dv_m = mathAgent.getDecisionVariable("m", "0");
			// expr.addTerm(1.0, dv_m);
			expr.addTerm(1.0, cachedDV_m);
			for (Integer t : tppa.processorToListOfTasksMap[p]) {
				// String indices = String.format("%d_%d", t, p);
				// DecisionVariable dv_y = mathAgent.getDecisionVariable("y",
				// indices);
				DecisionVariable dv_y = cacheDV_Y.get(t * problem.P + p);
				expr.addTerm(-problem.getETC(t, p), dv_y);
			}
			LPConstraint constraint = cachedConstraint[problem.T + p];
			constraint.setExpression(expr);
			mathAgent.addConstraint(constraint);
		}
	}

	private void generateConstraints2_FirstPass() {
		for (int p = 0; p < problem.P; p++) {
			String constraint_name = String.format("c2_p%d", p);
			LPExpression expr = new LPExpression();
			DecisionVariable dv_m = mathAgent.getIndexedDecisionVariable("m",
					"0");
			expr.addTerm(1.0, dv_m);
			cachedDV_m = dv_m;
			for (Integer t : tppa.processorToListOfTasksMap[p]) {
				// String indices = String.format("%d_%d", t, p);
				// DecisionVariable dv_y = mathAgent.getDecisionVariable("y",
				// indices);
				DecisionVariable dv_y = cacheDV_Y.get(t * problem.P + p);
				expr.addTerm(-problem.getETC(t, p), dv_y);
			}
			LPConstraint constraint = new LPConstraint(constraint_name);
			constraint.setExpression(expr);
			constraint.setLhs(0.0);
			constraint.setRhs(Double.MAX_VALUE);
			mathAgent.addConstraint(constraint);
			cachedConstraint[problem.T + p] = constraint;
		}
	}

	/**
	 * convert LP solution to IP solution using heuristics
	 * 
	 * @return
	 */
	public Solution getMathSolutionUsingHeuristics(TaskProcessorPairsAgent tppa) {
		Solution sol = new Solution(problem);
		for (int t = 0; t < problem.T; t++)
			for (Integer p : tppa.taskToListOfProcessorsMap[t]) {
				String indices = String.format("%s_%s", t, p);
				DecisionVariable dv = mathAgent.getIndexedDecisionVariable("y",
						indices);
				if (Math.abs(dv.getValue() - 1.0) < 0.001)
					sol.schedule(t, p);
			}
		return sol;
	}

	private Solution getInitialHeuristicSolution() {
//		HeuristicSolver solver = new HeuristicSolver(problem);
		MinMinPlus solver1 = new MinMinPlus(problem);
		Solution sol1 = solver1.solve();
//		Solution sol2 = solver.lsufferage();
//		Solution sol3 = solver.sufferage_variant2();
//		if (sol1.makespan() < sol2.makespan()
//				&& sol1.makespan() < sol3.makespan())
//			return sol1;
//		else if (sol2.makespan() < sol1.makespan()
//				&& sol2.makespan() < sol3.makespan())
//			return sol2;
//		else
//			return sol3;
		return sol1;
	}

	public String findLPBoundUsingColumnPricing() throws GRBException {
		Stopwatch timer = Stopwatch.createStarted();
		double bestLPBound = Double.MAX_VALUE;
		double previousBound = Double.MAX_VALUE;
		boolean lpfound = false;
		// [1] generate initial heuristic solution
		Solution solution = getInitialHeuristicSolution();
		log.info(solution.toString());

		// [2] select variables y_tp included in the solution and
		// randomly select other variables to be included
		initializeTppa(solution);
		mathAgent = new MathAgentGurobiCplex();
		mathAgent.setEnableOutput(false);
		mathAgent.setTimeLimit(20);
		mathAgent.setModelName("HCSP");
		int round = 0;
		do {
			round++;
			mathAgent.clearModel();
			formulateProblem();
			// mathAgent.printProblemLPFormat();
			mathAgent.printProblemStatistics();
			log.debug("SOLVE LP");
			if (solverType.equalsIgnoreCase("Gurobi")) {
				mathAgent.prepareGurobi();

				// dual simplex start
				mathAgent.getGurobiEnvironmentHook().set(GRB.IntParam.Sifting,
						2);
				mathAgent.getGurobiEnvironmentHook().set(
						GRB.IntParam.SiftMethod, 1);
				// dual simplex end

				mathAgent.solveUsingGurobi();
				mathAgent.computeDualsUsingGurobi();
				// mathAgent.print_X_RC_ForBasisVariables();
			} else if (solverType.equalsIgnoreCase("ORTools_GLPK")) {
				mathAgent.prepareORTools(MPSolver.OptimizationProblemType.GLPK_LINEAR_PROGRAMMING);
				boolean success = mathAgent.solveUsingORTools();
				if (!success)
					continue;
				mathAgent.computeDualsUsingORTools();
			} else if (solverType.equalsIgnoreCase("ORTools_CBC")) {
				mathAgent.prepareORTools(MPSolver.OptimizationProblemType.CLP_LINEAR_PROGRAMMING); //
				mathAgent.solveUsingORTools();
				mathAgent.computeDualsUsingORTools();
			} else {
				throw new IllegalStateException("Undefined solver");
			}
			int c1 = 0;
			for (DecisionVariable dv : mathAgent.getDecisionVariables("y")) {
				if (dv.getValue() > 0.9999)
					c1++;
			}

			solution = getIPSolutionUsingHeuristics();
			improve1(solution);
			improve2(solution);

			previousBound = bestLPBound;
			if (mathAgent.getObjectiveValue() - solution.makespan() > 0.0001) {
				bestLPBound = solution.makespan();
				if (previousBound != Double.MAX_VALUE) {
					double perc = -(bestLPBound - previousBound)
							/ previousBound;
					log.debug(String
							.format("Round %d==> %.6f%% (by IP Heuristic %.6f) Binary vars:%d Time passed:%s Previous obj=%.4f New obj=%.4f ",
									round,
									perc,
									(mathAgent.getObjectiveValue() - bestLPBound),
									c1, timer.toString(), previousBound,
									bestLPBound));
				}
				initializeTppa(solution);
			} else {
				bestLPBound = mathAgent.getObjectiveValue();
				if (previousBound != Double.MAX_VALUE) {
					double perc = -(bestLPBound - previousBound)
							/ previousBound;
					log.debug(String
							.format("Round %d==> %.6f%% Binary vars:%d Time passed:%s Previous obj=%.4f New obj=%.4f ",
									round, perc, c1, timer.toString(),
									previousBound, bestLPBound));
				}
				// remove non-basic variables
				List<DecisionVariable> to_be_removed = new ArrayList<>();
				for (DecisionVariable dv : mathAgent.getDecisionVariables("y")) {
					String status = null;
					if (solverType.equalsIgnoreCase("Gurobi")) {
						status = mathAgent
								.getBasisStatusForVariableUsingGurobi(dv);
						if (status.equalsIgnoreCase("AT_LOWER_BOUND")
								&& dv.getValue() < 0.0001)
							to_be_removed.add(dv);
					} else if (solverType.equalsIgnoreCase("ORTools_GLPK")) {
						status = mathAgent
								.getBasisStatusForVariableUsingORTools(dv);
						if (status.equalsIgnoreCase("AT_LOWER_BOUND")
								&& dv.getValue() < 0.0001)
							to_be_removed.add(dv);
					} else if (solverType.equalsIgnoreCase("ORTools_CBC")) {
						status = mathAgent
								.getBasisStatusForVariableUsingORTools(dv);
						if (status.equalsIgnoreCase("AT_LOWER_BOUND")
								&& dv.getValue() < 0.0001)
							to_be_removed.add(dv);
					}
				}
				for (DecisionVariable dv : to_be_removed) {
					String[] s = dv.getIndices().split("_");
					int t = Integer.parseInt(s[0]);
					int p = Integer.parseInt(s[1]);
					tppa.removePair(t, p);
				}
				log.debug(String
						.format("%d non basic AT_LOWER_BOUND variables having value < 0.0001 removed",
								to_be_removed.size()));

				// add variables based on dual values + reduced costs
				int vars_added = updateTppaBasedOnDualValues();

				if (solverType.equalsIgnoreCase("Gurobi")) {
					mathAgent.disposeUsingGurobi();
				} else if (solverType.equalsIgnoreCase("ORTools")) {
					mathAgent.disposeUsingORTools();
				}

				if (vars_added == 0) {
					log.debug("LP found. Number of variables with negative zero cost = 0");
					lpfound = true;
					break;
				}
			}
		} while (timer.elapsed(TimeUnit.SECONDS) < timeLimit);
		timer.stop();
		String rs = String
				.format("Time elapsed: %s Rounds: %d LP Solution found:%s LPBound:%.2f",
						timer.toString(), round, lpfound, bestLPBound);
		log.debug("Optimality conditions met : " + checkOptimalityConditions());
		log.debug(rs);
		return rs;
	}

	public void setYVarsLimit(int vars) {
		y_vars_limit = vars;
	}

	public void setExtraVarsLimit(int vars) {
		limit_vars_per_task = vars;
	}

	public void setSolverType(String solverType) {
		this.solverType = solverType;
	}

	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}
}

class TaskProcessorPairsAgent {
	List<Integer>[] taskToListOfProcessorsMap;
	List<Integer>[] processorToListOfTasksMap;
	int size = 0;

	public TaskProcessorPairsAgent(HCSProblem problem) {
		taskToListOfProcessorsMap = new ArrayList[problem.T];
		processorToListOfTasksMap = new ArrayList[problem.P];
		for (int i = 0; i < problem.T; i++)
			taskToListOfProcessorsMap[i] = new ArrayList<Integer>();
		for (int i = 0; i < problem.P; i++)
			processorToListOfTasksMap[i] = new ArrayList<Integer>();

	}

	public void removePair(int t, int p) {
		if (!taskToListOfProcessorsMap[t].contains(new Integer(p)))
			throw new IllegalStateException("remove pair failed");
		if (!processorToListOfTasksMap[p].contains(new Integer(t)))
			throw new IllegalStateException("remove pair failed");
		taskToListOfProcessorsMap[t].remove(new Integer(p));
		processorToListOfTasksMap[p].remove(new Integer(t));
		size--;
	}

	void addPair(int t, int p) {
		if (!taskToListOfProcessorsMap[t].contains(new Integer(p))) {
			size++;
			taskToListOfProcessorsMap[t].add(p);
		}
		if (!processorToListOfTasksMap[p].contains(new Integer(t))) {
			processorToListOfTasksMap[p].add(t);
		}
	}

	public boolean checkIfExists(int t, int p) {
		if (taskToListOfProcessorsMap[t].contains(new Integer(p)))
			return true;
		else
			return false;
	}

	public int getSize() {
		return size;
	}

	public void printStatistics() {
		int[] freq = new int[processorToListOfTasksMap.length + 1];
		int tot = 0;
		StringBuilder sb = new StringBuilder();
		for (int t = 0; t < taskToListOfProcessorsMap.length; t++) {
			freq[taskToListOfProcessorsMap[t].size()]++;
			tot++;
		}
		sb.append(String.format("{total vars=%d}", tot));

		for (int i = 0; i < freq.length; i++) {
			if (i == 1)
				sb.append(String.format("[Fixed->%d] ", freq[i]));
			else if (freq[i] > 0)
				sb.append(String.format("[%d->%d] ", i, freq[i]));
		}
		System.out.println(sb.toString());
	}
}

// class TaskProcessorPairsAgentOLD {
// Map<Integer, List<Integer>> taskToListOfProcessorsMap;
// Map<Integer, List<Integer>> processorToListOfTasksMap;
// int size = 0;
//
// public TaskProcessorPairsAgentOLD(HCSProblem problem) {
// taskToListOfProcessorsMap = new HashMap<>();
// processorToListOfTasksMap = new HashMap<>();
// }
//
// public void removePair(int t, int p) {
// if (!taskToListOfProcessorsMap.containsKey(new Integer(t))
// || !processorToListOfTasksMap.containsKey(new Integer(p)))
// throw new IllegalStateException("remove pair failed");
// if (!taskToListOfProcessorsMap.get(t).contains(new Integer(p))
// || !processorToListOfTasksMap.get(p).contains(new Integer(t)))
// throw new IllegalStateException("remove pair failed");
// taskToListOfProcessorsMap.get(t).remove(new Integer(p));
// processorToListOfTasksMap.get(p).remove(new Integer(t));
// size--;
// }
//
// void addPair(int t, int p) {
// if (!taskToListOfProcessorsMap.containsKey(new Integer(t)))
// taskToListOfProcessorsMap.put(t, new ArrayList<Integer>());
// if (!taskToListOfProcessorsMap.get(t).contains(new Integer(p))) {
// size++;
// taskToListOfProcessorsMap.get(t).add(p);
// }
// if (!processorToListOfTasksMap.containsKey(p))
// processorToListOfTasksMap.put(p, new ArrayList<Integer>());
// if (!processorToListOfTasksMap.get(p).contains(new Integer(t))) {
// processorToListOfTasksMap.get(p).add(t);
// }
// }
//
// public boolean checkIfExists(int t, int p) {
// if (!taskToListOfProcessorsMap.containsKey(new Integer(t)))
// return false;
// else if (!taskToListOfProcessorsMap.get(t).contains(new Integer(p)))
// return false;
// else
// return true;
// }
//
// public int getSize() {
// return size;
// }

// }

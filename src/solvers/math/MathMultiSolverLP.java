package solvers.math;

import gurobi.GRBException;
import ilog.concert.IloException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mathmodeler.DecisionVariable;
import mathmodeler.LPConstraint;
import mathmodeler.LPExpression;
import mathmodeler.MathAgent;
import mathmodeler.Objective;
import mathmodeler.MathAgentGurobiCplex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.google.ortools.linearsolver.MPSolver;

import core.HCSProblem;
import core.Importer;
import core.Solution;

public class MathMultiSolverLP {
	private static Logger log = LoggerFactory.getLogger(MathMultiSolverLP.class);

	public static void main(String[] args) throws IOException, GRBException, IloException {
		String fn = "datasets//Braun_et_al//u_c_hihi.0";
		// String fn = "datasets//Braun_et_al//u_s_lolo.0";
		// String fn = "datasets//1024x32//A.u_c_hihi";
		// String fn = "datasets//2048x64//A.u_c_hihi";
		// String fn = "datasets//4096x128//A.u_c_hihi";

		Importer importer = new Importer(fn);
		HCSProblem problem;
		problem = importer.read_dataset();
		MathMultiSolverLP app = new MathMultiSolverLP(problem);
		String s = app.findLPBoundUsingFullModel(200, "Gurobi");
		System.out.println(s);

	}

	HCSProblem problem;
	MathAgentGurobiCplex mathAgent;

	public MathAgentGurobiCplex getMathAgent() {
		return mathAgent;
	}

	public MathMultiSolverLP(HCSProblem problem) {
		this.problem = problem;
		mathAgent = new MathAgentGurobiCplex();
	}

	public void generateVariables1() {
		String key = "y";
		List<DecisionVariable> aList = new ArrayList<>();
		for (int t = 0; t < problem.T; t++)
			for (int p = 0; p < problem.P; p++) {
				String varname = String.format("y%d_%d", t, p);
				String indices = String.format("%d_%d", t, p);
				DecisionVariable dv = new DecisionVariable(varname, indices, "REAL");
				dv.setLb(0);
				dv.setUb(Double.MAX_VALUE);
				aList.add(dv);
			}
		mathAgent.addIndexedDecisionVariables(key, aList);
	}

	public void generateVariables2() {
		String key = "m";
		List<DecisionVariable> aList = new ArrayList<>();
		String varname = "m";
		String indices = "0";
		DecisionVariable dv = new DecisionVariable(varname, indices, "REAL");
		dv.setLb(0);
		dv.setUb(Double.MAX_VALUE);
		aList.add(dv);
		mathAgent.addIndexedDecisionVariables(key, aList);
	}

	public void generateObjective() {
		LPExpression expr = new LPExpression();
		DecisionVariable dv = mathAgent.getIndexedDecisionVariable("m", "0");
		expr.addTerm(1.0, dv);
		Objective objective = new Objective("Minimize");
		objective.setExpr(expr);
		mathAgent.setObjective(objective);
	}

	public void generateConstraints1() {
		for (int t = 0; t < problem.T; t++) {
			String constraint_name = String.format("c1_t%d", t);
			LPExpression expr = new LPExpression();
			for (int p = 0; p < problem.P; p++) {
				String indices = String.format("%d_%d", t, p);
				DecisionVariable dv = mathAgent.getIndexedDecisionVariable("y", indices);
				expr.addTerm(1.0, dv);
			}
			LPConstraint constraint = new LPConstraint(constraint_name);
			constraint.setExpression(expr);
			constraint.setLhs(1.0);
			constraint.setRhs(1.0);
			mathAgent.addConstraint(constraint);
		}
	}

	public void generateConstraints2() {
		for (int p = 0; p < problem.P; p++) {
			String constraint_name = String.format("c2_p%d", p);
			LPExpression expr = new LPExpression();
			DecisionVariable dv_m = mathAgent.getIndexedDecisionVariable("m", "0");
			expr.addTerm(-1.0, dv_m);
			for (int t = 0; t < problem.T; t++) {
				String indices = String.format("%d_%d", t, p);
				DecisionVariable dv_y = mathAgent.getIndexedDecisionVariable("y", indices);
				expr.addTerm(problem.getETC(t, p), dv_y);
			}
			LPConstraint constraint = new LPConstraint(constraint_name);
			constraint.setExpression(expr);
			constraint.setLhs(-Double.MAX_VALUE);
			constraint.setRhs(0.0);
			mathAgent.addConstraint(constraint);
		}
	}

	public void formulateProblem() {
		generateVariables1();
		generateVariables2();
		generateObjective();
		generateConstraints1();
		generateConstraints2();
	}

	public Solution getSolution() {
		Solution sol = new Solution(problem);
		for (int t = 0; t < problem.T; t++)
			for (int p = 0; p < problem.P; p++) {
				String indices = String.format("%s_%s", t, p);
				DecisionVariable dv = mathAgent.getIndexedDecisionVariable("y", indices);
				if (Math.abs(dv.getValue() - 1.0) < 0.001)
					sol.schedule(t, p);
			}
		return sol;
	}

	public String findLPBoundUsingFullModel(int timelimit, String solverType) throws GRBException {
		Stopwatch timer = Stopwatch.createStarted();
		mathAgent = new MathAgentGurobiCplex();
		mathAgent.setEnableOutput(false);
		mathAgent.setTimeLimit(timelimit);
		formulateProblem();
		double objvalue = Double.MAX_VALUE;
		if (solverType.equalsIgnoreCase("Gurobi")) {
			mathAgent.prepareGurobi();
			mathAgent.solveUsingGurobi();
			mathAgent.disposeUsingGurobi();
			objvalue = mathAgent.getObjectiveValue();
		} else if (solverType.equalsIgnoreCase("ORTOOLS_GLPK")) {
			MPSolver.OptimizationProblemType pt = MPSolver.OptimizationProblemType.valueOf("GLPK_LINEAR_PROGRAMMING");
			mathAgent.prepareORTools(pt);
			mathAgent.solveUsingORTools();
			objvalue = mathAgent.getObjectiveValue();
			mathAgent.disposeUsingORTools();
		} else if (solverType.equalsIgnoreCase("ORTOOLS_CBC")) {
			MPSolver.OptimizationProblemType pt = MPSolver.OptimizationProblemType.valueOf("CLP_LINEAR_PROGRAMMING");
			mathAgent.prepareORTools(pt);
			mathAgent.solveUsingORTools();
			objvalue = mathAgent.getObjectiveValue();
			mathAgent.disposeUsingORTools();
		} else if (solverType.equalsIgnoreCase("ORTOOLS_GLOP")) {
			MPSolver.OptimizationProblemType pt = MPSolver.OptimizationProblemType.valueOf("GLOP_LINEAR_PROGRAMMING");
			mathAgent.prepareORTools(pt);
			mathAgent.solveUsingORTools();
			objvalue = mathAgent.getObjectiveValue();
			mathAgent.disposeUsingORTools();
		}

		timer.stop();
		String rs = String.format("Time elapsed: %s LPBound:%.2f", timer.toString(), objvalue);
		log.debug(rs);
		return rs;
	}

}

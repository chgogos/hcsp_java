package solvers.math;

import gurobi.GRB;
import gurobi.GRBException;
import ilog.concert.IloException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mathmodeler.DecisionVariable;
import mathmodeler.LPConstraint;
import mathmodeler.LPExpression;
import mathmodeler.MathAgent;
import mathmodeler.Objective;
import mathmodeler.MathAgentGurobiCplex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.HCSProblem;
import core.Importer;
import core.Solution;

public class MathMultiSolverIP {
	private static Logger log = LoggerFactory.getLogger(MathMultiSolverIP.class);

	public static void main(String[] args) throws IOException, GRBException,
			IloException {
		String fn = "datasets//Braun_et_al//u_c_hihi.0";
		// String fn = "datasets//toy.0";
		Importer importer = new Importer(fn);
		HCSProblem problem;
		problem = importer.read_dataset();
		MathMultiSolverIP app = new MathMultiSolverIP(problem);
		app.formulateProblem();
		app.mathAgent.setModelName("HCSP");
		app.mathAgent.setEnableOutput(true);
		app.mathAgent.setTimeLimit(200);
		app.mathAgent.printProblemLPFormat();

		// {1} ORTOOLS GLPK
		// app.mathAgent.prepareORTools();
		// app.mathAgent.solveUsingORTools();
		// Solution sol = app.getSolution();
		// sol.printSolution();

		// {2} ORTOOLS CBC
		// app.mathAgent.prepareORTools(MPSolver.CBC_MIXED_INTEGER_PROGRAMMING);
		// System.out.println(app.mathAgent.getORToolsEnvironment()
		// .numConstraints());
		// app.mathAgent.solveUsingORTools();
		// Solution sol = app.getSolution();
		// sol.printSolution();

		// {3} GUROBI
		app.mathAgent.prepareGurobi();
		app.mathAgent.getGurobiEnvironmentHook().set(GRB.IntParam.Cuts, 2);
		app.mathAgent.getGurobiEnvironmentHook().set(GRB.IntParam.MIPFocus, 1);
		app.mathAgent.solveUsingGurobi();
		app.mathAgent.disposeUsingGurobi();
		Solution sol = app.getSolution();
		sol.printSolution();

		// {4} GUROBI
		// app.mathAgent.prepareGurobi();
		// app.mathAgent.getGurobiEnvironment().set(GRB.IntParam.Threads, 1);
		// app.mathAgent.getGurobiEnvironment().set(GRB.DoubleParam.MIPGap,
		// 0.01);
		// app.mathAgent.getGurobiEnvironment().set(GRB.IntParam.BranchDir, 1);
		// app.mathAgent.getGurobiEnvironment().set(GRB.DoubleParam.ImproveStartGap,
		// 0.05);
		// app.mathAgent.getGurobiEnvironment().set(GRB.DoubleParam.ImproveStartTime,
		// time_limit / 3);
		// app.mathAgent.getGurobiEnvironment().set(GRB.IntParam.VarBranch, 3);
		// app.mathAgent.getGurobiEnvironment().set(GRB.DoubleParam.Heuristics,
		// 0.2);
		// app.mathAgent.getGurobiEnvironment().set(GRB.IntParam.SubMIPNodes,
		// 1000);
		// app.mathAgent.solveUsingGurobi();
		// app.mathAgent.disposeUsingGurobi();
		// Solution sol = app.getSolution();
		// sol.printSolution();

		// {5} CPLEX
		// app.mathAgent.prepareCPLEX();
		// app.mathAgent.solveUsingCPLEX();
		// app.mathAgent.disposeUsingCPLEX();
		// Solution sol = app.getSolution();
		// sol.printSolution();

		// {6} CPLEX
		// app.mathAgent.prepareCPLEX();
		// app.mathAgent.getCPLEXEnvironment().setParam(IloCplex.IntParam.VarSel,
		// IloCplex.VariableSelect.Strong);
		// app.mathAgent.solveUsingCPLEX();
		// app.mathAgent.disposeUsingCPLEX();
		// Solution sol = app.getSolution();
		// sol.printSolution();

	}

	HCSProblem problem;
	MathAgentGurobiCplex mathAgent;

	public MathAgentGurobiCplex getMathAgent() {
		return mathAgent;
	}

	public MathMultiSolverIP(HCSProblem problem) {
		this.problem = problem;
		mathAgent = new MathAgentGurobiCplex();
	}

	public void generateVariables1() {
		String key = "y";
		List<DecisionVariable> aList = new ArrayList<>();
		for (int t = 0; t < problem.T; t++)
			for (int p = 0; p < problem.P; p++) {
				String varname = String.format("y%d_%d", t, p);
				String indices = String.format("%d_%d", t, p);
				DecisionVariable dv = new DecisionVariable(varname, indices,
						"BINARY");
				aList.add(dv);
			}
		mathAgent.addIndexedDecisionVariables(key, aList);
	}

	public void generateVariables2() {
		String key = "m";
		List<DecisionVariable> aList = new ArrayList<>();
		String varname = "m";
		String indices = "0";
		DecisionVariable dv = new DecisionVariable(varname, indices, "REAL");
		dv.setLb(0);
		dv.setUb(Double.MAX_VALUE);
		aList.add(dv);
		mathAgent.addIndexedDecisionVariables(key, aList);
	}

	public void generateObjective() {
		LPExpression expr = new LPExpression();
		DecisionVariable dv = mathAgent.getIndexedDecisionVariable("m", "0");
		expr.addTerm(1.0, dv);
		Objective objective = new Objective("Minimize");
		objective.setExpr(expr);
		mathAgent.setObjective(objective);
	}

	public void generateConstraints1() {
		for (int t = 0; t < problem.T; t++) {
			String constraint_name = String.format("c1_t%d", t);
			LPExpression expr = new LPExpression();
			for (int p = 0; p < problem.P; p++) {
				String indices = String.format("%d_%d", t, p);
				DecisionVariable dv = mathAgent.getIndexedDecisionVariable("y",
						indices);
				expr.addTerm(1.0, dv);
			}
			LPConstraint constraint = new LPConstraint(constraint_name);
			constraint.setExpression(expr);
			constraint.setLhs(1.0);
			constraint.setRhs(1.0);
			mathAgent.addConstraint(constraint);
		}
	}

	public void generateConstraints2() {
		for (int p = 0; p < problem.P; p++) {
			String constraint_name = String.format("c2_p%d", p);
			LPExpression expr = new LPExpression();
			DecisionVariable dv_m = mathAgent.getIndexedDecisionVariable("m", "0");
			expr.addTerm(-1.0, dv_m);
			for (int t = 0; t < problem.T; t++) {
				String indices = String.format("%d_%d", t, p);
				DecisionVariable dv_y = mathAgent.getIndexedDecisionVariable("y",
						indices);
				expr.addTerm(problem.getETC(t, p), dv_y);
			}
			LPConstraint constraint = new LPConstraint(constraint_name);
			constraint.setExpression(expr);
			constraint.setLhs(-Double.MAX_VALUE);
			constraint.setRhs(0.0);
			mathAgent.addConstraint(constraint);
		}
	}

	public void formulateProblem() {
		generateVariables1();
		generateVariables2();
		generateObjective();
		generateConstraints1();
		generateConstraints2();
	}

	public Solution getSolution() {
		Solution sol = new Solution(problem);
		for (int t = 0; t < problem.T; t++)
			for (int p = 0; p < problem.P; p++) {
				String indices = String.format("%s_%s", t, p);
				DecisionVariable dv = mathAgent.getIndexedDecisionVariable("y",
						indices);
				if (Math.abs(dv.getValue() - 1.0) < 0.001)
					sol.schedule(t, p);
			}
		return sol;
	}

}

package solvers.math;

import mathmodeler.ORToolsManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPSolver.ResultStatus;
import com.google.ortools.linearsolver.MPVariable;

import core.HCSProblem;
import core.Solution;

public class MathSolverDirectSolveIPUsingORTools {
	private static Logger log = LoggerFactory
			.getLogger(MathSolverDirectSolveIPUsingORTools.class);
	HCSProblem problem;

	MPSolver ortools_solver;
	MPVariable[][] y;
	MPVariable m;

	static {
		ORToolsManager.loadLibraries();
	}

	public MathSolverDirectSolveIPUsingORTools(HCSProblem problem) {
		this.problem = problem;
	}

	public void solve() {
		setupSolverORTools();
		generateVariablesORTools();
		generateObjectiveORTools();
		generateConstraintsORTools();
		solveORTools();
	}

	private void setupSolverORTools() {
		ortools_solver = new MPSolver("hcsp",
				MPSolver.OptimizationProblemType.GLPK_MIXED_INTEGER_PROGRAMMING);
//		log.info(ortools_solver.solverVersion());
		ortools_solver.enableOutput();
		ortools_solver.setTimeLimit(1000 * 30);
	}

	private void generateVariablesORTools() {
		y = new MPVariable[problem.T][problem.P];
		for (int t = 0; t < problem.T; t++)
			for (int p = 0; p < problem.P; p++) {
				String yvarname = String.format("y%d_%d", t, p);
				y[t][p] = ortools_solver.makeIntVar(0, 1, yvarname);
			}
		m = ortools_solver.makeNumVar(0, Double.MAX_VALUE, "m");
	}

	private void generateObjectiveORTools() {
		ortools_solver.objective().setCoefficient(m, 1);
		ortools_solver.objective().setMinimization();
	}

	private void generateConstraintsORTools() {
		for (int t = 0; t < problem.T; t++) {
			String constraint_name = String.format("c1_t%d", t);
			MPConstraint oexpr = ortools_solver.makeConstraint(1, 1,
					constraint_name);
			for (int p = 0; p < problem.P; p++) {
				oexpr.setCoefficient(y[t][p], 1);
			}
		}

		for (int p = 0; p < problem.P; p++) {
			String constraint_name = String.format("c2_p%d", p);
			MPConstraint oexpr = ortools_solver.makeConstraint(
					-MPSolver.infinity(), 0, constraint_name);
			oexpr.setCoefficient(m, -1);
			for (int t = 0; t < problem.T; t++) {
				oexpr.setCoefficient(y[t][p], problem.getETC(t, p));
			}
		}
		System.out.println("Number of constraints = "
				+ ortools_solver.numConstraints());
	}

	private void solveORTools() {
		double scaleFactor = 1.0;
		ResultStatus ret = ortools_solver.solve();
		if (ret == MPSolver.ResultStatus.ABNORMAL || ret == MPSolver.ResultStatus.INFEASIBLE
				|| ret == MPSolver.ResultStatus.UNBOUNDED || ret == MPSolver.ResultStatus.NOT_SOLVED) {
			log.error("Solver terminated abnormally");
			ortools_solver.clear();
			ortools_solver.delete();
			return;
		}
		double obj = ortools_solver.objective().value() / scaleFactor;
		if (obj >= 1E50) {
			log.error("Solver value is too high");
			ortools_solver.clear();
			ortools_solver.delete();
			return;
		}

		double objBound = ortools_solver.objective().bestBound() / scaleFactor;
		double gap = 100 * obj / objBound - 100.0;

		log.info("ORT_IPsolver value = " + obj);
		log.info("ORT_IPsolver bound = "
				+ ortools_solver.objective().bestBound());
		log.info("ORT_IPsolver gap = " + gap);
		Solution sol = new Solution(problem);
		for (int t = 0; t < problem.T; t++)
			for (int p = 0; p < problem.P; p++) {
				if (Math.abs(y[t][p].solutionValue() - 1.0) <= 0.0001)
					sol.schedule(t, p);
			}
		log.info(String.format("Makespan = %.2f\n", sol.makespan()));
		ortools_solver.clear();
		ortools_solver.delete();
	}
}

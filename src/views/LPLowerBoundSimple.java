package views;

import java.io.IOException;

import core.HCSProblem;
import core.Importer;
import gurobi.GRBException;
import solvers.math.MathMultiSolverLP;

public class LPLowerBoundSimple {

	static {
		System.loadLibrary("jniortools");
	}
	
	public static void main(String[] args) throws IOException, GRBException {
		Importer importer = new Importer("datasets//Braun_et_al//u_s_lolo.0");
		HCSProblem problem = importer.read_dataset();
		MathMultiSolverLP mmslp = new MathMultiSolverLP(problem);
		System.out.println("ORTOOLS GLPK " + mmslp.findLPBoundUsingFullModel(100, "ORTOOLS_GLPK"));
		System.out.println("ORTOOLS CBC  " + mmslp.findLPBoundUsingFullModel(100, "ORTOOLS_CBC"));
		System.out.println("ORTOOLS GLOP " + mmslp.findLPBoundUsingFullModel(100, "ORTOOLS_GLOP"));
		System.out.println("GUROBI       " + mmslp.findLPBoundUsingFullModel(100, "gurobi"));
	}

}

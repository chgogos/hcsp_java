package views;

import gurobi.GRB;
import gurobi.GRBException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import mathmodeler.ORToolsManager;
import solvers.math.MathMultiSolverIP;

import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPSolver.OptimizationProblemType;

import core.HCSProblem;
import core.Importer;
import core.Solution;

public class IPLowerBoundsForBraunDatasetsView {

	static {
		ORToolsManager.loadLibraries();
	}

	public static void main(String[] args) throws IOException, GRBException {
		IPLowerBoundsForBraunDatasetsView app = new IPLowerBoundsForBraunDatasetsView();
		Scanner scanner = new Scanner(System.in);
		System.out.printf("1. Batch solve Braun datasets using Gurobi\n");
		System.out.printf("2. Batch solve Braun datasets using ORTOOLS CBC\n");
		System.out.printf("3. Batch solve Braun datasets using ORTOOLS GLPK\n");
		// System.out
		// .printf("4. Batch solve Braun datasets for using ORTOOLS SCIP\n");
		System.out.printf("5. Solve dataset using GUROBI\n");

		System.out.print("Enter choice: ");
		int choice = scanner.nextInt();
		System.out.print("Enter time limit in seconds: ");
		app.timelimit = scanner.nextInt();

		if (choice == 1) {
			app.runAllBraunDatasetsUsingGurobi();
			// app.processDatasetUsingGurobi(new File(
			// "datasets//Braun_et_al//u_c_hihi.0"));
		} else if (choice == 2) {
			app.runAllBraunDatasetsUsingORTools(MPSolver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING);
		} else if (choice == 3) {
			app.runAllBraunDatasetsUsingORTools(MPSolver.OptimizationProblemType.GLPK_MIXED_INTEGER_PROGRAMMING);
		}
		// else if (choice == 4) {
		// app.runAllBraunDatasetsUsingORTools(MPSolver.SCIP_MIXED_INTEGER_PROGRAMMING);
		// }
		else if (choice == 5) {
//			app.processDatasetUsingGurobi("datasets//1024x32//A.u_c_hihi");
			app.processDatasetUsingGurobi("datasets//8192x256//A.u_c_hihi");
		}
		scanner.close();
	}

	int timelimit;

	public void runAllBraunDatasetsUsingGurobi() throws IOException,
			GRBException {
		File folder = new File("datasets//Braun_et_al");
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);
		for (File fileEntry : listOfFiles) {
			if (fileEntry.isFile()) {
				processDatasetUsingGurobi(fileEntry);
			}
		}
	}

	public void processDatasetUsingGurobi(File fileEntry) throws IOException,
			GRBException {
		if (fileEntry.isFile()) {
			Importer importer = new Importer(fileEntry.getAbsolutePath());
			HCSProblem problem;
			problem = importer.read_dataset();
			MathMultiSolverIP solver = new MathMultiSolverIP(problem);
			solver.formulateProblem();
			solver.getMathAgent().setModelName("HCSP");
			solver.getMathAgent().setEnableOutput(false);
			solver.getMathAgent().setTimeLimit(timelimit);
			solver.getMathAgent().prepareGurobi();
			// simple pricing is 3 (Quick-Start Steepest Edge)
			solver.getMathAgent().getGurobiEnvironmentHook()
					.set(GRB.IntParam.SimplexPricing, 3);
			solver.getMathAgent().solveUsingGurobi();
			double objval = solver.getMathAgent().getGurobiModelHook()
					.get(GRB.DoubleAttr.ObjVal);
			double objBound = solver.getMathAgent().getGurobiModelHook()
					.get(GRB.DoubleAttr.ObjBound);
			System.out.printf("%s ==> MIP=%.2f LB=%.2f GAP=%.4f\n",
					fileEntry.getName(), objval, objBound, 100
							* (objval - objBound) / objBound);
			solver.getMathAgent().disposeUsingGurobi();
			Solution sol = solver.getSolution();
			String fn = String
					.format("datasets//solutions//%s_[%.2f]_[LB=%.2f]_[%dsec]_Gurobi.sol",
							fileEntry.getName(), sol.makespan(), objBound,
							timelimit);
			sol.exportToFile(fn);
			// sol.printSolution();

		}
	}

	public void processDatasetUsingGurobi(String fn) throws IOException,
			GRBException {
		Importer importer = new Importer(fn);
		HCSProblem problem;
		problem = importer.read_dataset();
		MathMultiSolverIP solver = new MathMultiSolverIP(problem);
		solver.formulateProblem();
		solver.getMathAgent().setModelName("HCSP");
		solver.getMathAgent().setEnableOutput(true);
		solver.getMathAgent().setTimeLimit(timelimit);
		solver.getMathAgent().prepareGurobi();
		// simple pricing is 3 (Quick-Start Steepest Edge)
		solver.getMathAgent().getGurobiEnvironmentHook()
				.set(GRB.IntParam.SimplexPricing, 3);
		solver.getMathAgent().solveUsingGurobi();
		double objval = solver.getMathAgent().getGurobiModelHook()
				.get(GRB.DoubleAttr.ObjVal);
		System.out.println(objval);
		solver.getMathAgent().disposeUsingGurobi();

	}

	public void runAllBraunDatasetsUsingORTools(OptimizationProblemType cbcMixedIntegerProgramming)
			throws IOException {
		File folder = new File("datasets//Braun_et_al");
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);
		for (File fileEntry : listOfFiles) {
			if (fileEntry.isFile()) {
				processDatasetUsingORTools(fileEntry, cbcMixedIntegerProgramming);
			}
		}
	}

	public void processDatasetUsingORTools(File fileEntry, OptimizationProblemType cbcMixedIntegerProgramming)
			throws IOException {
		if (fileEntry.isFile()) {
			Importer importer = new Importer(fileEntry.getAbsolutePath());
			HCSProblem problem;
			problem = importer.read_dataset();
			MathMultiSolverIP solver = new MathMultiSolverIP(problem);
			solver.formulateProblem();
			solver.getMathAgent().setModelName("HCSP");
			solver.getMathAgent().setEnableOutput(false);
			solver.getMathAgent().setTimeLimit(timelimit);
			solver.getMathAgent().prepareORTools(cbcMixedIntegerProgramming);
			solver.getMathAgent().solveUsingORTools();
			double objval = solver.getMathAgent().getORToolsHook().objective()
					.value();
			double objBound = solver.getMathAgent().getORToolsHook()
					.objective().bestBound();
			System.out.printf("%s ==> MIP=%.2f LB=%.2f GAP=%.4f\n",
					fileEntry.getName(), objval, objBound, 100
							* (objval - objBound) / objBound);
			solver.getMathAgent().disposeUsingORTools();
			Solution sol = solver.getSolution();
			String solverTypeS;
			if (cbcMixedIntegerProgramming == MPSolver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING)
				solverTypeS = "CBC";
			else if (cbcMixedIntegerProgramming == MPSolver.OptimizationProblemType.GLPK_MIXED_INTEGER_PROGRAMMING)
				solverTypeS = "GLPK";
			// else if (solverType == MPSolver.SCIP_MIXED_INTEGER_PROGRAMMING)
			// solverTypeS = "SCIP";
			else
				throw new IllegalStateException("Unknown solver " + cbcMixedIntegerProgramming);
			String fn = String.format(
					"datasets//solutions//%s_[%.2f]_[LB=%.2f]_[%dsec]_%s.sol",
					fileEntry.getName(), sol.makespan(), objBound, timelimit,
					solverTypeS);
			sol.exportToFile(fn);
			// sol.printSolution();

		}

	}
}

package views;

import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.renderer.SimpleTextRenderer;
import gurobi.GRBException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.google.common.base.Stopwatch;

import mathmodeler.MathAgent;
import mathmodeler.ORToolsManager;
import solvers.math.MathSolverLPColumnPricing;
import core.HCSProblem;
import core.Importer;

public class LPLowerBoundsView {
	static {
		ORToolsManager.loadLibraries();
	}

	final static long random_seed = 1234567890L;
	private static final EtmMonitor etmMonitor = EtmManager.getEtmMonitor();
	private static EtmMonitor monitor;

	public static void main(String[] args) throws IOException, GRBException {
		BasicEtmConfigurator.configure();
		monitor = EtmManager.getEtmMonitor();
		monitor.start();

		LPLowerBoundsView app = new LPLowerBoundsView();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
		String solverType;
		int choice;
		StringBuilder prequel = new StringBuilder();
		String nameOS = "os.name";
		String versionOS = "os.version";
		String architectureOS = "os.arch";
		String archDataModelOS = "sun.arch.data.model";
		prequel.append(String.format("Started at: %s\n", sdf.format(new Date())));
		prequel.append(String.format("OS: %s\n", System.getProperty(nameOS)));
		prequel.append(String.format("OS Version: %s\n",
				System.getProperty(versionOS)));
		prequel.append(String.format("OS Architecture: %s\n",
				System.getProperty(architectureOS)));
		prequel.append(String.format("OS 32 or 64 bit: %s\n",
				System.getProperty(archDataModelOS)));
		prequel.append(String.format("Processors: %d\n", Runtime.getRuntime()
				.availableProcessors()));

		Scanner in = new Scanner(System.in);

		String[] foldernames = { "datasets//Braun_et_al", "datasets//1024x32",
				"datasets//2048x64", "datasets//4096x128", "datasets//8192x256" };

		System.out.println("1. Gurobi");
		System.out.println("2. ORTOOLS GLPK");
		System.out.println("3. ORTOOLS CBC");
		System.out.print("Choose solver: ");
		choice = in.nextInt();
		if (choice == 1)
			solverType = "Gurobi";
		else if (choice == 2)
			solverType = "ORTOOLS_GLPK";
		else if (choice == 3)
			solverType = "ORTOOLS_CBC";
		else {
			in.close();
			throw new IllegalStateException("Unknown solver");
		}
		prequel.append(String.format("Solver type: %s\n", solverType));

		System.out
				.print("Enter time limit in seconds for each problem (default 300): ");
		int time_limit = in.nextInt();
		prequel.append(String.format(
				"Time limit for each problem:%d seconds\n", time_limit));

		System.out.println("1. Batch run all datasets LP column  pricing");
		System.out.println("2. Batch run all datasets Full LP (not ready)");
		System.out.print("Choose: ");
		choice = in.nextInt();
		List<String> selectedGroupOfDatasets = new ArrayList<String>();
		prequel.append("Selected datasets:\n");
		for (String foldername : foldernames) {
			System.out.printf("Include %s datasets (y/n)? ", foldername);
			String cchoice = in.next();
			if (cchoice.equalsIgnoreCase("y")) {
				selectedGroupOfDatasets.add(foldername);
				prequel.append(foldername + "\n");
			}
		}
		if (choice == 1) {
			String efn = String.format(
					"datasets//solutions//LBs(Column Pricing)_%s_%s.txt",
					sdf.format(new Date()), solverType);
			prequel.append(String.format("Random seed: %s\n", random_seed));

			System.out
					.print("Enter maximum number of task variables (Y) in each subproblem (default 20000): ");
			int limit_y_vars = in.nextInt();
			prequel.append(String.format(
					"Limit of variables for each LP subproblem: %d\n",
					limit_y_vars));
			System.out
					.print("Enter limit of variables included for each task (default 20): ");
			int limit_vars_per_task = in.nextInt();
			prequel.append(String.format(
					"Limit of variables included for each task: %d\n",
					limit_vars_per_task));
			appendStringToFile(prequel.toString(), efn);

			for (String foldername : selectedGroupOfDatasets) {
				appendStringToFile("\n" + foldername, efn);
				app.batchEvaluationUsingColumnPricing(efn, foldername,
						limit_y_vars, limit_vars_per_task, solverType,
						time_limit);
			}
			appendStringToFile(String.format("\nFinished at: %s\n",
					sdf.format(new Date())), efn);
		} else if (choice == 2) {
			// String efn =
			// String.format("datasets//solutions//LBs(Full Model)_%s_%s_%s.txt",
			// solverType, sdf.format(new Date()));
			//
		}

		in.close();
		etmMonitor.render(new SimpleTextRenderer());
		monitor.stop();
	}

	public void batchEvaluationUsingColumnPricing(String resultsfilename,
			String foldername, int vars, int extra_vars, String solverType,
			int timeLimit) throws IOException, GRBException {
		Stopwatch timer = Stopwatch.createStarted();
		File folder = new File(foldername);
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);
		appendStringToFile(String.format("Files: %d", listOfFiles.length),
				resultsfilename);
		int i = 0;
		for (File fileEntry : listOfFiles) {
			if (fileEntry.isFile()) {
				System.out.println(fileEntry.getAbsolutePath());
				Importer importer = new Importer(fileEntry.getAbsolutePath());
				HCSProblem problem = importer.read_dataset();
				MathSolverLPColumnPricing app = new MathSolverLPColumnPricing(
						problem, random_seed);
				app.setYVarsLimit(vars);
				app.setExtraVarsLimit(extra_vars);
				app.setSolverType(solverType);
				app.setTimeLimit(timeLimit);
				i++;
				String line = String.format("%d) %s %s", i,
						fileEntry.getName(),
						app.findLPBoundUsingColumnPricing());
				appendStringToFile(line, resultsfilename);
			}
		}
		appendStringToFile("Time elapsed: " + timer.toString(), resultsfilename);
	}

	// public void batchEvaluationFullLP(String fn)
	// throws IOException, GRBException {
	// List<String> results = new ArrayList<>();
	// File folder = new File("datasets//" + fn);
	// File[] listOfFiles = folder.listFiles();
	// Arrays.sort(listOfFiles);
	// for (File fileEntry : listOfFiles) {
	// if (fileEntry.isFile()) {
	// System.out.println(fileEntry.getAbsolutePath());
	// Importer importer = new Importer(fileEntry.getAbsolutePath());
	// HCSProblem problem = importer.read_dataset();
	// MathMultiSolverLP app = new MathMultiSolverLP(problem);
	// results.add(fileEntry.getName() + " "
	// + app.findLPBoundUsingFullModel(3600, solverType));
	// }
	// BufferedWriter pw = new BufferedWriter(new FileWriter(efn));
	// for (String s : results) {
	// System.out.println(s);
	// pw.write(s + "\n");
	// }
	// pw.close();
	// }
	//
	// }

	public static void appendStringToFile(String line, String resultsfilename) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(resultsfilename, true)));
			out.print(line);
			out.print(System.getProperty("line.separator"));
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

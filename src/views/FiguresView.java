package views;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.google.common.base.Stopwatch;

import core.HCSProblem;
import core.Importer;
import core.Solution;
import solvers.heuristics.MinMinNaive;
import solvers.heuristics.MinMinPlus;

public class FiguresView {

	public static void main(String[] args) throws IOException {
		FiguresView app = new FiguresView();
		// app.fig1(new File("datasets//toy.0"));
		app.fig2(100);

	}

	/**
	 * uncomment log.debug in HeuristicSolver
	 * 
	 * scheduleFastBasedOnStaticOrder
	 * 
	 * 
	 * @param fe
	 * @return
	 */
	public String fig1(File fe) {
		StringBuilder sb = new StringBuilder();
		Importer importer = new Importer(fe.getAbsolutePath());
		HCSProblem problem;
		try {
			problem = importer.read_dataset();
			// System.out.println(problem.toString());
			// problem.display();
			MinMinPlus solver = new MinMinPlus(problem);
			sb.append(String.format("%s MinMinFast=%.1f\n", fe.getName(), solver.solve().makespan()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

	/**
	 * minminfast vs minmin time comparison
	 * 
	 * @throws IOException
	 */
	public void fig2(int repetitions) throws IOException {
		// String[] paths = { "datasets//Braun_et_al"};
		String[] paths = { "datasets//Braun_et_al", "datasets//1024x32", "datasets//2048x64", "datasets//4096x128",
				"datasets//8192x256" };

		StringBuilder sb = new StringBuilder();
		Stopwatch timer = Stopwatch.createStarted();
		for (String path : paths) {
			DescriptiveStatistics stats = new DescriptiveStatistics();
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			Arrays.sort(listOfFiles);
			for (File fileEntry : listOfFiles) {
				if (fileEntry.isFile()) {
					Importer importer = new Importer(fileEntry.getAbsolutePath());
					HCSProblem problem = importer.read_dataset();
					MinMinNaive solver1 = new MinMinNaive(problem);
					MinMinPlus solver2 = new MinMinPlus(problem);
					timer.reset();
					timer.start();
					double sum1 = 0.0;
					for (int i = 0; i < repetitions; i++) {
						Solution sol1 = solver1.solve();
						sum1 += sol1.makespan();
					}
					long t1 = timer.elapsed(TimeUnit.MILLISECONDS);
					timer.reset();
					timer.start();
					double sum2 = 0.0;
					for (int i = 0; i < repetitions; i++) {
						Solution sol2 = solver2.solve();
						sum2 += sol2.makespan();
					}
					long t2 = timer.elapsed(TimeUnit.MILLISECONDS);
					double times = t1 / (double) t2;
					stats.addValue(times);
					String rs = String.format("%s %.2f %.2f MM:%d MMF:%d Times Quicker:%.2f",
							fileEntry.getAbsoluteFile(), sum1, sum2, t1, t2, times);
					sb.append(rs + "\n");
					System.out.println(rs);
				}
			}
			sb.append(String.format("Times faster Mean:%.2f Std:%.2f Max:%.2f Min:%.2f\n", stats.getMean(),
					stats.getStandardDeviation(), stats.getMax(), stats.getMin()));
		}
		System.out.println(sb.toString());
	}

}

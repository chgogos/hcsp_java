package views;

import java.io.IOException;

import core.HCSProblem;
import core.Importer;
import core.Solution;
import solvers.heuristics.MinMinPlus;

public class ManyObjectives {

	public static void main(String[] args) {
		ManyObjectives mo = new ManyObjectives();
		mo.testManyObjectives();
	}

	public void testManyObjectives() {
		StringBuilder sb = new StringBuilder();
		// Importer importer = new Importer("datasets//toy.0");
		Importer importer = new Importer("datasets//1024x32//A.u_s_lolo");
		// Importer importer = new
		// Importer("datasets//Braun_et_al//u_c_hihi.0");

		HCSProblem problem;
		try {
			problem = importer.read_dataset();
			MinMinPlus solver = new MinMinPlus(problem);
			Solution sol = solver.solve();
			sb.append(String.format("MinMin makespan=%.1f\n", sol.makespan()));
			sb.append(String.format("MinMin flowtime=%.1f\n", sol.flowTime()));
			sb.append(String.format("MinMin resourceutilisation=%.1f\n",
					sol.resourceUtilization()));
			sb.append(String.format("MinMin matchingproximity=%.1f\n",
					sol.matchingProximity()));

		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(sb.toString());
	}
}

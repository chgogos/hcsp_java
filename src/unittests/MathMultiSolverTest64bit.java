package unittests;

import static org.junit.Assert.assertEquals;
import gurobi.GRBException;
import ilog.concert.IloException;
import solvers.math.MathMultiSolverIP;

import java.io.IOException;

import org.junit.Test;

import com.google.ortools.linearsolver.MPSolver;

import core.HCSProblem;
import core.Importer;

public class MathMultiSolverTest64bit {

	@Test
	public void testIPObjectiveValueOnSmallDataset() {
		String fn = "datasets//toy.0";
		Importer importer = new Importer(fn);
		HCSProblem problem;
		try {
			problem = importer.read_dataset();
			MathMultiSolverIP app = new MathMultiSolverIP(problem);
			app.formulateProblem();
			app.getMathAgent().setModelName("HCSP");
			app.getMathAgent().setEnableOutput(false);

			app.getMathAgent().prepareORTools();
			app.getMathAgent().solveUsingORTools();
			app.getMathAgent().disposeUsingORTools();
			assertEquals(22.0, app.getMathAgent().getObjectiveValue(), 0.01);

			app.getMathAgent().prepareORTools(
					MPSolver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING);
			app.getMathAgent().solveUsingORTools();
			app.getMathAgent().disposeUsingORTools();
			assertEquals(22.0, app.getMathAgent().getObjectiveValue(), 0.01);

//			app.getMathAgent().prepareORTools(
//					MPSolver.SCIP_MIXED_INTEGER_PROGRAMMING);
//			app.getMathAgent().solveUsingORTools();
//			app.getMathAgent().disposeUsingORTools();
//			assertEquals(22.0, app.getMathAgent().getObjectiveValue(), 0.01);

			app.getMathAgent().prepareGurobi();
			app.getMathAgent().solveUsingGurobi();
			app.getMathAgent().disposeUsingGurobi();
			assertEquals(22.0, app.getMathAgent().getObjectiveValue(), 0.01);

			// app.getMathAgent().prepareCPLEX();
			// app.getMathAgent().solveUsingCPLEX();
			// app.getMathAgent().disposeUsingCPLEX();
			// assertEquals(22.0, app.getMathAgent().getObjectiveValue(), 0.01);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (GRBException e) {
			e.printStackTrace();
		} 
//		catch (IloException e) {
//			e.printStackTrace();
//		}

	}
}

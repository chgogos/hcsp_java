package unittests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;

import core.HCSProblem;
import core.Importer;
import solvers.heuristics.MinMinNaive;
import solvers.heuristics.MinMinPlus;

public class HeuristicSolversTest {
	// String fn = "datasets//8192x256//A.u_c_hihi";
	// assertEquals(solver.minmin().makespan(), 14798375.7, 0.1);

	HCSProblem problem;

	@Rule
	public TestRule benchmarkRun = new BenchmarkRule();

	@Before
	public void setUp() throws Exception {
		String fn = "datasets//Braun_et_al//u_c_hihi.0";
		Importer importer = new Importer(fn);
		problem = importer.read_dataset();

	}

	@BenchmarkOptions(benchmarkRounds = 1000, warmupRounds = 10)
	@Test
	public void testMinMinBraun_et_al_u_c_hihi_0() {
		MinMinNaive solver = new MinMinNaive(problem);
		assertEquals(solver.solve().makespan(), 8460675.0, 0.1);
	}

	@BenchmarkOptions(benchmarkRounds = 1000, warmupRounds = 10)
	@Test
	public void testMinMinFastBraun_et_al_u_c_hihi_0() {
		MinMinPlus solver = new MinMinPlus(problem);
		assertEquals(solver.solve().makespan(), 8460675.0, 0.1);
	}

}

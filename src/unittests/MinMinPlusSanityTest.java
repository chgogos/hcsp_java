package unittests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.HCSProblem;
import core.Importer;
import solvers.heuristics.MinMin;
import solvers.heuristics.MinMinNaive;
import solvers.heuristics.MinMinPlus;

public class MinMinPlusSanityTest {
	private static Logger log = LoggerFactory.getLogger(MinMinPlusSanityTest.class);
	MinMinNaive solver1;
	MinMinPlus solver2;
	MinMin solver3;

	@Test
	public void testSanity() {
		// String[] paths = { "datasets//Braun_et_al", "datasets//1024X32",
		// "datasets//2048X64", "datasets//4096X128",
		// "datasets//4096X128", "datasets//8192x256" };
		String[] paths = { "datasets//4096X128", "datasets//4096X128", "datasets//8192x256" };
		for (String path : paths) {
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			for (File fileEntry : listOfFiles) {
				if (fileEntry.isFile()) {
					Importer importer = new Importer(fileEntry.getAbsolutePath());
					HCSProblem problem;
					try {
						problem = importer.read_dataset();
						solver1 = new MinMinNaive(problem);
						double solver1_makespan = solver1.solve().makespan();
						solver2 = new MinMinPlus(problem);
						double solver2_makespan = solver2.solve().makespan();
						solver3 = new MinMin(problem);
						double solver3_makespan = solver3.solve().makespan();
						log.info(String.format("%s//%s %f <--> %f <--> %f", path, fileEntry.getName(), solver1_makespan,
								solver2_makespan, solver3_makespan));
						assertEquals(solver1_makespan, solver2_makespan, 0.1);
						assertEquals(solver1_makespan, solver3_makespan, 0.1);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

	}
}

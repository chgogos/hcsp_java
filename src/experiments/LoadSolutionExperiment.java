package experiments;

import java.io.IOException;
import core.HCSProblem;
import core.Importer;
import core.Solution;

public class LoadSolutionExperiment {

	public static void main(String[] args) {
		LoadSolutionExperiment app = new LoadSolutionExperiment();
//		app.test_solution("datasets\\1024x32\\A.u_c_hihi", "1024x32_A.u_c_hihi_19520527.3.sol");
		
		// solution produced by sit project incorrectly reports that makespan is 3406 but is 3428.5
//		app.test_solution("datasets\\Braun_et_al\\u_s_lolo.0", "datasets\\solutions\\problematic\\u_s_lolo.0.3406");
		
		
		app.test_solution("datasets\\Braun_et_al\\u_s_lolo.0", "datasets\\solutions\\problematic\\u_s_lolo.0.3421");
	}

	void test_solution(String dataset_fn, String solution_fn) {
		Importer importer = new Importer(dataset_fn);
		try {
			HCSProblem problem = importer.read_dataset();
			Solution solution = new Solution(problem);
			solution.loadFromFile(solution_fn);
			solution.printSolutionObjectives();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
package experiments;

import java.io.IOException;

import core.HCSProblem;
import core.Importer;
import core.Solution;
import solvers.heuristics.MinMin;

public class MinMinMatchingProximityExperiment {

	public static void main(String[] args) {
		Importer importer = new Importer("datasets//Braun_et_al//u_c_hihi.0");
		HCSProblem problem;
		try {
			problem = importer.read_dataset();
			MinMin mm = new MinMin(problem);
			Solution sol = mm.solve();
			sol.matchingProximity();
			System.out.printf("%.1f %.4f\n", sol.matchingProximity(), 1.0/sol.matchingProximity());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}

package experiments;

import java.io.IOException;

import core.HCSProblem;
import core.Importer;
import core.Solution;
import solvers.heuristics.Sufferage;
import solvers.heuristics.Sufferage2;
import solvers.heuristics.Sufferage3;

public class SufferageExperiment {

	public static void main(String[] args) throws IOException {
		Importer importer = new Importer("datasets//Braun_et_al//u_c_hihi.0");
		HCSProblem problem = importer.read_dataset();
		Sufferage suf1 = new Sufferage(problem);
		Solution sol1 = suf1.solve();
		System.out.printf("Sufferage1 = %.1f\n", sol1.makespan());
		Sufferage2 suf2 = new Sufferage2(problem);
		Solution sol2 = suf2.solve();
		System.out.printf("Sufferage2 = %.1f\n", sol2.makespan());
		Sufferage3 suf3 = new Sufferage3(problem);
		Solution sol3 = suf3.solve();
		System.out.printf("Sufferage3 = %.1f\n", sol3.makespan());
	}

}

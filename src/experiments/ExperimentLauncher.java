package experiments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import core.HCSProblem;
import core.Importer;
import core.Solution;
import solvers.Solver;

public class ExperimentLauncher {
	private static Logger log = LoggerFactory.getLogger(ExperimentLauncher.class);

	final static Locale locale = Locale.ENGLISH;

	// final static Locale locale = new Locale("el", "GR ");

	public static void main(String[] args) {
		ExperimentLauncher app = new ExperimentLauncher();
		try {
			if (args.length == 0) {
				// app.launch("experiment1.txt");
				// app.launch("experiment_chaturvedi.txt");
				app.launch("experiment1_50trials.txt", 10, "ms");
//				app.launch("experiment2_50trials.txt", 10, "s");
//				app.launch("experiment3_50trials.txt", 10, "s");
//				app.launch("experiment4_20trials.txt", 2, "s");
//				app.launch("experiment5_10trials.txt", 2, "s");
			} else
				app.launch(args[0], 0, "s");
		} catch (IOException | NoSuchMethodException | SecurityException | IllegalAccessException
				| ClassNotFoundException | InstantiationException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	int trials;
	String heuristics;
	String results_csv;
	String dataset_folder;
	String lower_bounds_file;
	Map<String, Double> linear_programming_lower_bounds;

	public void launch(String exp_fn, int warmup, String t)
			throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException, ClassNotFoundException {
		Properties prop = new Properties();
		InputStream input;
		input = new FileInputStream(exp_fn);
		prop.load(input);
		trials = Integer.parseInt(prop.getProperty("trials"));
		dataset_folder = prop.getProperty("dataset_folder");
		lower_bounds_file = prop.getProperty("lower_bounds_file");
		linear_programming_lower_bounds = read_lower_bounds(lower_bounds_file);
		heuristics = prop.getProperty("heuristics");
		results_csv = prop.getProperty("results_csv");
		log.info(String.format("trials=%d;dataset_folder=%s;heuristics=%s;export_file_name=%s", trials, dataset_folder,
				heuristics, results_csv));
		File[] listOfFiles = new File(dataset_folder).listFiles();
		// shuffleArray(listOfFiles);
		String[] heuristic_names = heuristics.split(",");
		CSVWriter writer = new CSVWriter(new FileWriter(results_csv), ';');
		List<String> header = new ArrayList<String>();
		header.add("dataset");
		for (int j = 0; j < heuristic_names.length; j++) {
			header.add(heuristic_names[j]);
			if (trials > 1)
				header.add(heuristic_names[j] + "_time");
		}
		header.add("LPLB");
		String[] header_s = new String[header.size()];
		header.toArray(header_s);
		writer.writeNext(header_s, false);
		for (int i = 0; i < listOfFiles.length; i++) {
			String[] line;
			if (trials == 1)
				line = new String[heuristic_names.length + 2];
			else
				line = new String[heuristic_names.length * 2 + 2];
			line[0] = listOfFiles[i].getName();
			log.info("Processing: " + line[0]);
			// System.out.println(listOfFiles[i]);
			Importer importer = new Importer(listOfFiles[i].getAbsolutePath());
			HCSProblem problem = importer.read_dataset();
			for (int j = 0; j < heuristic_names.length; j++) {
				Class<?> cl = Class.forName("solvers.heuristics." + heuristic_names[j]);
				Constructor<?> cons = cl.getConstructor(HCSProblem.class);
				Solver solver = (Solver) cons.newInstance(problem);
				double temp = 0.0;
				for (int k = 0; k < warmup; k++) {
					Solution sol = solver.solve();
					temp += sol.makespan();
				}
				long time = System.nanoTime();
				double makespan = 0.0;
				for (int k = 0; k < trials; k++) {
					Solution sol = solver.solve();
					makespan = sol.makespan();
				}
				long elapsed_time_ns = (System.nanoTime() - time) / trials;
				double elapsed_time_ms = elapsed_time_ns / 1000000.0;
				double elapsed_time_s = elapsed_time_ms / 1000.0;
				if (trials == 1)
					line[j + 1] = String.format(locale, "%.1f", makespan);
				else {
					line[2 * j + 1] = String.format(locale, "%.1f", makespan);
					if (t.equalsIgnoreCase("ms"))
						line[2 * j + 2] = String.format(locale, "%.4f", elapsed_time_ms);
					else
						line[2 * j + 2] = String.format(locale, "%.4f", elapsed_time_s);
				}
				line[line.length - 1] = String.format(locale, "%.1f", linear_programming_lower_bounds.get(line[0]));
			}

			writer.writeNext(line, false);
		}
		writer.close();
		printToConsole(results_csv);
	}

	private Map<String, Double> read_lower_bounds(String lower_bounds_file) {
		Map<String, Double> lbs = new HashMap<>();
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(lower_bounds_file), ';', CSVParser.DEFAULT_QUOTE_CHARACTER, 1);
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				lbs.put(nextLine[0], Double.parseDouble(nextLine[1]));
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lbs;
	}

	private void printToConsole(String results_csv) {
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(results_csv), ';');
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				for (int i = 0; i < nextLine.length; i++) {
					if (i == 0)
						System.out.print(nextLine[i]);
					else
						System.out.print(Strings.padStart(nextLine[i], 20, ' '));
				}
				System.out.println();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Implementing Fisher�Yates shuffle
	void shuffleArray(File[] ar) {
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			File a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

}

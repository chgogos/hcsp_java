package experiments;

import java.io.IOException;

import core.HCSProblem;
import core.Importer;
import core.Solution;
import solvers.heuristics.MinMinPlus;

public class MinMinPlusExperiment {

	public static void main(String[] args) throws IOException {
		Importer importer = new Importer("datasets//Braun_et_al//u_c_hihi.0");
		HCSProblem problem = importer.read_dataset();
		MinMinPlus mmp = new MinMinPlus(problem);
//		mmp.printPriorityQueues();
		Solution sol = mmp.solve();
		System.out.println(sol.makespan());
	}

}

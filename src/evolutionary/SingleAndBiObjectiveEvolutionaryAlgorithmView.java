package evolutionary;

import java.io.IOException;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.moeaframework.Analyzer;
import org.moeaframework.Executor;
import org.moeaframework.Instrumenter;
import org.moeaframework.analysis.collector.Accumulator;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;

public class SingleAndBiObjectiveEvolutionaryAlgorithmView {

	public static void main(String[] args) {
		// String fn = "datasets//Braun_et_al//u_c_hihi.0";
		String fn = "datasets//Braun_et_al//u_c_lolo.0"; // LB 5135.7
		// String fn = "datasets//1024x32//A.u_s_lolo";
		// String fn = "datasets//8192x256//A.u_s_lolo";
		String[] algorithms = { "eMOEA", "NSGAII", "eNSGAII", "MOEAD", "GDE3",
				"Random", "AbYSS", "CellDE", "DENSEA", "FastPGA", "IBEA",
				"MOCell", "OMOPSO", "PAES", "PESA2", "SMPSO", "SPEA2" };
		// "MOCHC", "SMSEMOEA"

		String algorithm = algorithms[1]; // NSGAII

		int maxEvaluations = 50000;

		System.out.printf("Dataset: %s Algorithm: %s Max evaluations:%d\n", fn,
				algorithm, maxEvaluations);
		System.out.printf("1. Single objective (makespan)\n");
		System.out.printf("2. Two objectives (makespan, flowtime)\n");
		System.out
				.printf("3. Test single objective (makespan) all algorithms\n");
		System.out.printf("4. Instrumenter (error!)\n");
		System.out.printf("5. Analyzer\n");
		System.out.printf("Enter choice: ");
		Scanner scanner = new Scanner(System.in);
		int ch = scanner.nextInt();
		SingleAndBiObjectiveEvolutionaryAlgorithmView ea = new SingleAndBiObjectiveEvolutionaryAlgorithmView();
		if (ch == 1) {
			ea.testSingleObjective(fn, algorithm, maxEvaluations);
		} else if (ch == 2) {
			ea.testBiObjective(fn, algorithm, maxEvaluations);
		} else if (ch == 3) {
			double min = Double.MAX_VALUE;
			String algomin = "";
			for (String algo : algorithms) {
				System.out.printf("%s started\n", algo);
				double ov = ea.testSingleObjective(fn, algo, maxEvaluations);
				if (ov < min) {
					min = ov;
					algomin = algo;
				}
			}
			System.out.printf("Winner: %s value: %.2f\n", algomin, min);
		} else if (ch == 4) {
			ea.testBiObjectiveWithInstrumenter(fn, algorithm, maxEvaluations);
		} else if (ch == 5) {
			ea.testBiObjectiveWithAnalyzer(fn, algorithm, maxEvaluations);
		}
		scanner.close();
	}

	void testBiObjectiveWithAnalyzer(String fn, String algorithm,
			int maxEvaluations) {
		Analyzer analyzer = new Analyzer()
				.withProblemClass(EvolutionaryHcspBiObjective.class, fn)
				.includeAllMetrics().showStatisticalSignificance();

		Executor executor = new Executor().withProblemClass(
				EvolutionaryHcspBiObjective.class, fn).withMaxEvaluations(
				maxEvaluations);

		analyzer.addAll("NSGAII", executor.withAlgorithm("NSGAII").runSeeds(50));
		analyzer.addAll("GDE3", executor.withAlgorithm("GDE3").runSeeds(50));

		try {
			analyzer.printAnalysis();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void testBiObjectiveWithInstrumenter(String fn, String algorithm,
			int maxEvaluations) {
		Instrumenter instrumenter = new Instrumenter()
				.withProblemClass(EvolutionaryHcspBiObjective.class, fn)
				.withFrequency(100).attachAll();

		NondominatedPopulation result = new Executor()
				.withProblemClass(EvolutionaryHcspBiObjective.class, fn)
				.withAlgorithm(algorithm).withMaxEvaluations(maxEvaluations)
				.withInstrumenter(instrumenter).distributeOnAllCores().run();

		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double objectives[] = solution.getObjectives();
			System.out.printf("Solution makespan=%.2f flowtime=%.2f\n",
					objectives[0], objectives[1]);
		}

		Accumulator accumulator = instrumenter.getLastAccumulator();
		for (int i = 0; i < accumulator.size("NFE"); i++) {
			System.out.println(accumulator.get("NFE", i) + "\t"
					+ accumulator.get("Elapsed Time", i) + "\t"
					+ accumulator.get("GenerationalDistance", i));
		}
	}

	double testSingleObjective(String fn, String algorithm, int maxEvaluations) {
		NondominatedPopulation result = new Executor()
				.withProblemClass(EvolutionaryHcspSingleObjective.class, fn)
				.withAlgorithm(algorithm).withMaxEvaluations(maxEvaluations)
				.distributeOnAllCores().run();
		if (result.size() > 1)
			System.out.println("More than one results");
		Solution solution = result.get(0);
		double objectives[] = solution.getObjectives();
		System.out.printf("Solution %.2f\n", objectives[0]);
		return objectives[0];
	}

	void testBiObjective(String fn, String algorithm, int maxEvaluations) {
		NondominatedPopulation result = new Executor()
				.withProblemClass(EvolutionaryHcspBiObjective.class, fn)
				.withAlgorithm(algorithm).withMaxEvaluations(maxEvaluations)
				.distributeOnAllCores().run();
		double values[][] = new double[result.size()][2];
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double objectives[] = solution.getObjectives();
			System.out.printf("Solution makespan=%.2f flowtime=%.2f\n",
					objectives[0], objectives[1]);
			values[i][0] = objectives[0];
			values[i][1] = objectives[1];
		}
		showGraph(values);
	}

	private static void showGraph(double[][] values) {
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries data = new XYSeries("Makespan vs Flowtime");
		for (int i = 0; i < values.length; i++) {
			double x0 = values[i][0]; // makespan
			double x1 = values[i][1]; // flowtime
			data.add(x0, x1);
		}
		dataset.addSeries(data);
		final JFreeChart chart = createChart(dataset);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		final ApplicationFrame frame = new ApplicationFrame("Title");
		frame.setContentPane(chartPanel);
		frame.pack();
		frame.setVisible(true);
	}

	private static JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart chart = ChartFactory.createScatterPlot(
				"Makespan vs Flowtime chart", // title
				"Makespan 1", // x axis label
				"Flowtime", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);
		XYPlot plot = (XYPlot) chart.getPlot();
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesLinesVisible(0, true);
		plot.setRenderer(renderer);
		return chart;
	}

}

java -cp "bin;lib/*" experiments.ExperimentLauncher experiment1.txt
"C:\Program Files\R\R-3.2.1\bin\i386\Rscript" heuristics_experiments.R heuristics_arena_512X16.csv heuristics_arena_512X16.pdf "Datasets 512x16"
java -cp "bin;lib/*" experiments.ExperimentLauncher experiment2.txt
"C:\Program Files\R\R-3.2.1\bin\i386\Rscript" heuristics_experiments.R heuristics_arena_1024X32.csv heuristics_arena_1024X32.pdf "Datasets 1024x32"
java -cp "bin;lib/*" experiments.ExperimentLauncher experiment3.txt
"C:\Program Files\R\R-3.2.1\bin\i386\Rscript" heuristics_experiments.R heuristics_arena_2048X64.csv heuristics_arena_2048X64.pdf "Datasets 2048x64"
java -cp "bin;lib/*" experiments.ExperimentLauncher experiment4.txt
"C:\Program Files\R\R-3.2.1\bin\i386\Rscript" heuristics_experiments.R heuristics_arena_4096X128.csv heuristics_arena_4096X128.pdf "Datasets 4096x128"
java -cp "bin;lib/*" experiments.ExperimentLauncher experiment5.txt
"C:\Program Files\R\R-3.2.1\bin\i386\Rscript" heuristics_experiments.R heuristics_arena_8192X256.csv heuristics_arena_8192X256.pdf "Datasets 8192x256"
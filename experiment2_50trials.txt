trials=50
dataset_folder=datasets//1024X32
lower_bounds_file=datasets//1024X32_LBs.txt
heuristics= MinMin,MinMax,RelativeCost,Sufferage,LSufferage,PenaltyBased,TPB
results_csv=heuristics_arena_1024X32_50trials.csv